import crisprbact


def test_on_target_predict_empty():
    # Empty sequence
    predicted_target = crisprbact.on_target_predict("")
    assert len(predicted_target) == 0, "the list is non empty"


def test_on_target_predict_size_guide():
    size_guide = 20
    predicted_targets = crisprbact.on_target_predict(
        """ TGCCTGTTTACGCGCCGATTGTTGCG
        AGATTTGGACGGACGTTGACGGGG
        TCTATACCTGCGACCCGCGTCAGG
        TGCCCGATGCGAGGTTGTTGAAGT
        CGATGTCCTACCAGGAAGCGATGG
        AGCTTTCCTACTTCGGCG"""
    )
    guides = (predicted_target["guide"] for predicted_target in predicted_targets)
    for guide in guides:
        assert len(guide) == size_guide, (
            "the guide do not have a length of " + size_guide
        )
    pams = (
        (predicted_target["pam"], predicted_target["start"])
        for predicted_target in predicted_targets
    )
    for pam in pams:
        (pam_val, start_val) = pam
        assert (
            start_val - pam_val == 3
        ), "the difference between start and pam position is different than 3"


def test_smaller_target_seq():
    size_guide = 20
    seq = "TTTTTTTTTTTTTTTTCCAGAAAAGAAAAAAAAAAAAAC"
    predicted_targets = crisprbact.on_target_predict(seq)
    list_predicted_targets = list(predicted_targets)
    assert len(list_predicted_targets) == 1, "Should have only 1 result"
    for predicted_target in list_predicted_targets:
        guide = predicted_target["guide"]
        assert guide == "GTTTTTTTTTTTTTCTTTTC", "matching guide"
        assert (
            predicted_target["target"] == "CTTTTCTGGAAAAAAAAAAAAAAAA"
        ), "the predicted target"
        assert predicted_target["start"] == 19, "position start"
        assert predicted_target["stop"] == 39, "position stop"
        assert predicted_target["pam"] == 16, "PAM pos"
        assert len(guide) == size_guide, (
            "the guide do not have a length of " + size_guide
        )
